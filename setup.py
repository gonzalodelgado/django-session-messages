from distutils.core import setup
from distutils.command.install import INSTALL_SCHEMES

setup(
    name = 'django-session-messages',
    version = '0.1.1',
    description = 'Anonymous session-based messages for Django',
    author = 'Carl Meyer',
    author_email = 'carl@dirtcircle.com',
    url = 'http://code.google.com/p/django-session-messages/',
    packages = ['session_messages'],
    classifiers = ['Development Status :: 4 - Beta',
                   'Environment :: Web Environment',
                   'Framework :: Django',
                   'Intended Audience :: Developers',
                   'License :: OSI Approved :: BSD License',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                   'Topic :: Utilities'],
)