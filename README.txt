=======================
Django Session Messages
=======================

This is an anonymous session messages application for Django projects

For installation instructions, see the file "INSTALL.txt" in this
directory; for instructions on how to use this application, and on
what it provides, see the file "overview.txt" in the "docs/"
directory.
